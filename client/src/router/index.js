import { createRouter, createWebHistory } from 'vue-router';
import authMiddleware from './middleware/auth';
import { routes } from './routes';

const router = createRouter({
     history: createWebHistory(process.env.BASE_URL),
     routes,
});

router.beforeEach(authMiddleware);

export default router;
