import jwt from 'jsonwebtoken';
import Token from '@Root/models/Token';
import { IToken } from '@Root/interfaces';
import config from '@Root/config/config';

const generateResetToken = async (email: string): Promise<IToken> => {
     const resetPasswordToken: string = jwt.sign({ email }, config.JWT_SECRET_KEY, { expiresIn: config.JWT_RESET_EXPIRATION });

     const tokenData = {
          token: resetPasswordToken,
          type: 'RESET_TOKEN',
          user_email: email,
     };

     return await new Token(tokenData).save();
};

export default {
     generateResetToken,
};
