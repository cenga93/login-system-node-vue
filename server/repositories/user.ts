import { Request } from 'express';
import httpStatus from 'http-status';
import ApiError from '@Root/utils/ApiError';
import { getOne, isExists } from '@Root/default';
import User, { IUserModel } from '@Root/models/User';
import { sendWelcomeMail } from '@Root/services/mailer';
import { IUser } from '@Root/interfaces';

/** CREATE NEW USER */
const createUser = async (req: Request): Promise<IUser> => {
     const { body } = req;

     // Check if user exists in database
     const userExists: boolean = await isExists(User, { email: body.email });
     if (userExists) throw new ApiError(httpStatus.FORBIDDEN, 'User already exists');

     // Generate code for account verification
     body.code = Math.round(Math.random() * (9999 - 1000) + 1000);

     // Save new user in database
     const newUser: IUserModel = await new User(body).save();

     // Send welcome mail to new user email
     await sendWelcomeMail(newUser);

     return await newUser.getPublicFields();
};

/** VERIFYING  USER */
const verifyUser = async (_id: string, code: boolean): Promise<IUser> => {
     const currentUser: IUserModel = await getOne(User, { _id, code });

     // Check if user exists in database
     if (!currentUser) throw new ApiError(httpStatus.FORBIDDEN, 'User not found');
     if (currentUser.verified) throw new ApiError(httpStatus.FORBIDDEN, 'User is already verified');

     // Verifying user
     Object.assign(currentUser, { verified: true });

     // Update  user in database
     await currentUser.save();

     return await currentUser.getPublicFields();
};

export default {
     createUser,
     verifyUser,
};
