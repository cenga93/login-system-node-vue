export const Permissions = {
     user: {
          CREATE: 'user:create',
          READ: 'user:read',
          UPDATE: 'user:update',
          DELETE: 'user:delete',
     },
};
