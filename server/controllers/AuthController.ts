import { catchAsync } from 'catch-async-express';
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import authRepository from '@Root/repositories/auth';
import User, { IUserModel } from '@Root/models/User';
import ApiError from '@Root/utils/ApiError';
import { getOne } from '@Root/default';

export const login = catchAsync(async (req: Request, res: Response): Promise<void> => {
     const data: { user: IUserModel; token: string } = await authRepository.login(req.body);

     res.cookie('token', data.token, {
          httpOnly: true,
          secure: true,
     });

     res.status(httpStatus.OK).json({ ...data.user._doc });
});

export const resetPassword = catchAsync(async (req: Request, res: Response): Promise<void> => {
     await authRepository.resetPassword(req.body.email);

     res.status(httpStatus.OK).json({
          message: 'success',
     });
});

export const updatePassword = catchAsync(async (req: Request, res: Response): Promise<void> => {
     const token: any = req.query.token;
     const password: string = req.body.password;

     await authRepository.updatePassword(token, password);

     res.status(httpStatus.OK).json({
          message: 'success',
     });
});

export const me = catchAsync(async (req: Request | any, res: Response): Promise<void> => {
     const user: IUserModel = await getOne(User, { email: req.user.email });
     if (!user) throw new ApiError(httpStatus.FORBIDDEN, 'User not found');

     const publicUserData = await user.getPublicFields();

     res.status(httpStatus.OK).json(publicUserData);
});
