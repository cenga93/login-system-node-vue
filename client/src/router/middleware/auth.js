import store from '@/store';
import router from '@/router';

export default ({ meta }, from, next) => {
     if (meta.auth) {
          if (!store.getters.getUser) {
               router.push({ name: 'LoginView' }).then(() => {});
          } else {
               next();
          }
     } else {
          if (store.getters.getUser) {
               router.push({ name: 'DashboardView' }).then(() => {});
          } else {
               next();
          }
     }
};
